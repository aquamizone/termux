#!/bin/sh
# 
# Copyleft 2019 - Iron Six
# Licensed under GNU GPL V3
#

echo "Creating backup folder"
mkdir ~/backup-mizone
mkdir ~/backup-mizone/.config
echo "Done..."
echo
echo
clear

iron_six () {
date 
echo "Installing necessary package"
if ! type "aria2c" > /dev/null; then
   pkg install aria2
fi
if ! type "bash" > /dev/null; then
   pkg install bash
fi
if ! type "mogrify" > /dev/null; then
   pkg install imagemagick ghostscript
fi
if ! type "ranger" > /dev/null; then
   pkg install ranger
fi
if ! type "vim" > /dev/null; then
   pkg install vim
fi
if ! type "7z" > /dev/null; then
   pkg install p7zip
fi
if ! type "tar" > /dev/null; then
   pkg install tar
fi
if ! type "unzip" > /dev/null; then
   pkg install unzip
fi
if ! type "unrar" > /dev/null; then
   pkg install unrar
fi
echo "Done..."
echo
echo
echo "Press [Return] to continue"; read -r "AA"

echo "Backup current configurations"
cp -rv ~/.config/ranger ~/backup-mizone/.config/
cp -v ~/.vimrc ~/backup-mizone/
echo "Done..."
echo "Backup loacated at"
echo "~/backup-mizone"
echo
echo
echo "Press [Return] to continue"; read -r "AA"

echo "Deleting old configuration files"
rm -rfv ~/.config/ranger
rm -v ~/.vimrc
echo "Done..."
echo "Backup loacated at"
echo "~/backup-mizone"
echo
echo
echo "Press [Return] to continue"; read -r "AA"

echo "Creating symlink from dotfiles"
ln -sv $PWD/.config/ranger ~/.config/
ln -sv $PWD/.vimrc ~/
echo "Done..."
echo
echo
echo "Press [Return] to continue"; read -r "AA"

echo "Configuring stuff"
chsh -s bash
mkdir -v ~/.vim
mkdir -v ~/.vim/backup
mkdir -v ~/.vim/backup/undo
termux-setup-storage
echo "Done..."
echo
echo
echo "Press [Return] to continue"; read -r "AA"

read -p "Install torrent client? [y/N] " response
case $response in [yY][eE][sS]|[yY]|'') 
date
echo "Installing torrent server, transmission"
if ! type "curl" > /dev/null; then
   pkg install curl
fi
if ! type "transmission-daemon" > /dev/null; then
   pkg install transmission
fi
echo "Backup transmission configurations"
cp -rv ~/.config/transmission-daemon ~/backup-mizone/.config/
echo "Deleting old transmission configurations"
rm -rfv ~.config/transmission-daemon
echo "Configuring transmission"
ln -sv $PWD/.config/transmission-daemon ~/.config/
mkdir -v ~/.config/transmission-daemon/{resume,blacklists,torrents}
echo
echo
echo "Installing torrent client, tremc"
if ! type "tremc" > /dev/null; then
mkdir -v /data/data/com.termux/files/usr/tmp/mizone
cd /data/data/com.termux/files/usr/tmp/mizone
git clone -v https://github.com/tremc/tremc /data/data/com.termux/files/usr/tmp/mizone/tremc
mv -v /data/data/com.termux/files/usr/tmp/mizone/tremc/tremc /data/data/com.termux/files/usr/bin/
chmod -v 755 /data/data/com.termux/files/usr/bin/tremc
chmod -v +x /data/data/com.termux/files/usr/bin/tremc
fi
echo "Cleaning installation"
rm -rfv /data/data/com.termux/files/usr/tmp/mizone
echo "Configuring torrent client"
mkdir -v ~/torrent
mkdir -v ~/torrent/done
mkdir -v ~/torrent/temp
chmod -Rv 775 ~/torrent
echo "Done..."
echo 
echo
echo "/!\ Notice"
echo "To start torrent service"
echo "run \"transmission-daemon\""
echo
echo "To stop torrent service"
echo "run \"transmission-remote --exit\""
echo
echo "To use torrent client"
echo "Access via web GUI, http://127.0.0.1:9091"
echo "Or run \"tremc\""
echo
echo
echo "/!\ Torrent dir information"
echo "Watch dir ~/torrent (place torrent file here)"
echo "Completed dir ~/torrent/done"
echo "Incomplete dir ~/torrent/temp"
echo
echo "Press [Return] to continue"; read -r "AA"
;;
*)
echo "Canceled..."
echo 
echo 
;;
esac
}
iron_six 2>&1 | tee ~/backup-mizone/log.txt

echo "Log created at ~/backup-mizone/log.txt"
echo "All done..."
echo
echo "/!\ Please restart Termux"
echo "Press [Return] to exit"; read -r "AA"
