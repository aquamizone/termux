set noswapfile
set backupdir=~/.vim/backup/
set directory=~/.vim/backup/

syntax on
set showcmd
filetype plugin indent off

set number
set showcmd showbreak=-|-
set undofile
set undodir=~/.vim/backup/undo/
set lazyredraw
set linebreak

set wrap
set tabstop=3 shiftwidth=3
set expandtab
set backspace=indent,eol,start
set mouse=r

set hlsearch
set incsearch
set ignorecase
set smartcase

noremap <F12> :set invnumber<CR>

set laststatus=2

hi User1 ctermbg=white ctermfg=black   guibg=white guifg=black
hi User2 ctermbg=blue ctermfg=white   guibg=blue guifg=white
hi User3 ctermbg=green ctermfg=black   guibg=green guifg=black

set statusline=
set statusline+=%1*\ [%n]
set statusline+=%3*\ [%<%1{getcwd()}]
set statusline+=%2*\ %<%{expand('%')}\ %=
set statusline+=%1*\ [LINE:\ %l/%L]
set statusline+=%1*\ [COL:\ %c] 
