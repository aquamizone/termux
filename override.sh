#!/bin/sh
# 
# Copyleft 2019 - Iron Six
# Licensed under GNU GPL V3
#

iron_six () {
date 
echo "Getting current user"
user=$(whoami)
echo ""
echo ""
echo "Overriding permission"
chown -vR $user:$user ~/.config/ranger
chown -vR $user:$user ~/.config/transmission-daemon
chown -vR $user:$user ~/.vimrc
echo ""
echo ""
echo "Fixing permission"
chmod -vR 755 ~/.config/ranger
chmod -vR 755 ~/.config/transmission-daemon
chmod -vR 755 ~/.vimrc
echo "Done..."
}

iron_six 2>&1 | tee ~/backup-mizone/log_override.txt

echo "All done..."
echo ""
echo "Press [Return] to exit"; read -r "AA"
